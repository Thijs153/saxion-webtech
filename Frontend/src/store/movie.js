import {derived, writable} from "svelte/store";
import api from "../scripts/api.js";
import {paginate} from "../scripts/pagination";
import router from "page";
import errorStore from "./error.js";
import { notifier } from '@beyonk/svelte-notifications'

const rawMovies = writable([]);
const searchTerm = writable("");
const status = writable("")

const filteredMovies = derived(
    [searchTerm, status, rawMovies],
    ([$searchTerm, $status, $items]) => {
        let items = $items;

        if($status === "sellable" ){
            return items.filter(x => x.title.toLowerCase().includes($searchTerm.toLowerCase()) && !x.isSold)
        }

        if($status === "sold") {
            return items.filter(x => x.title.toLowerCase().includes($searchTerm.toLowerCase()) && x.isSold)
        }

        return items.filter(x => x.title.toLowerCase().includes($searchTerm.toLowerCase()))
    }
)

const createMovie = (data) => {
    if(!validateMovie(data)) { return }

    api.post("/movies", data)
        .then(responseData => {
            let tempMovies = [];

            rawMovies.subscribe(data => {
                tempMovies = data;
            })

            tempMovies.push(responseData);
            setRawMovies(tempMovies);

            errorStore.setError(null);

            notifier.success("Movie Created")
            router.redirect("/movies")
        });
}

const deleteMovie = (movieID) => {

    api.del(`/movies/${movieID}`)
        .then(() => {
            let tempMovies = [];

            rawMovies.subscribe(data => {
                tempMovies = data.filter(item => item.id !== movieID);
            })

            errorStore.setError(null);

            setRawMovies(tempMovies);
        });
}

const paginatedMovieList = (page, perPage) => {
    let movies = [];
    const unsubscribe = filteredMovies.subscribe(value => {
        movies = value
    })

    unsubscribe();

    if(movies.length <= 0) return []
    return paginate(movies, page, perPage);
}

const fetchData = async() => {
    let $storeValues;
    rawMovies.subscribe($ => $storeValues = $);

    if($storeValues.length > 0) {
        return $storeValues
    } else {
        return api.get('/movies')
            .then(response => {
                setRawMovies(response)
            })
    }
}

const validateMovie = (data) => {
    if(!data.title || !data.description || !data.category || !data.lengthInMinutes || !data.yearOfRelease || !data.rating) {
        const errorMessage = "All fields are required!";
        errorStore.setError(errorMessage);
        return false;
    }

    //Min length => 0
    if(data.lengthInMinutes <= 0) {
        const errorMessage = "Length can not be or less than 0!";
        errorStore.setError(errorMessage);
        return false;
    }

    //Max length 300 => 5 hours
    if(data.lengthInMinutes > 300) {
        const errorMessage = "Length can not be longer than 300 minutes (5 hours)!";
        errorStore.setError(errorMessage);
        return false;
    }


    //Release year can't be before the year in which the first movie ever got created ( Src: Google :o )
    if(data.yearOfRelease < 1888) {
        const errorMessage = "The first ever movie was created in 1888, so the release year can't be less than that!";
        errorStore.setError(errorMessage);
        return false;
    }

    //Release year can't be after current year.
    const currentYear = new Date().getFullYear();
    if(data.yearOfRelease > currentYear) {
        const errorMessage = "Release year can not be higher than the current year!"
        errorStore.setError(errorMessage);
        return false;
    }

    //Min rating 0
    if(data.rating < 0) {
        const errorMessage = "Rating can not be lower than 0!";
        errorStore.setError(errorMessage);
        return false;
    }

    //Max rating 10
    if(data.rating > 10) {
        const errorMessage = "Rating can not be higher than 10!";
        errorStore.setError(errorMessage);
        return false;
    }

    return true;
}

const setRawMovies = (data) => {
    rawMovies.set(data);
}

const getMovies = () => {
    return rawMovies;
}

const setSearchTerm = (term) => {
    searchTerm.set("term");
}

const getSearchTerm = () => {
    return searchTerm;
}


const getFilteredMovies = () => {
    return filteredMovies
}

export default {
    fetchData,
    rawMovies,
    setRawMovies,
    getMovies,
    getFilteredMovies,
    getSearchTerm,
    setSearchTerm,
    paginatedMovieList,
    searchTerm,
    status,
    createMovie,
    deleteMovie

}