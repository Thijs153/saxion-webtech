import {v4 as uuid} from "uuid";

/**
 * User Entity
 * Created by Mark Scheperman and Thijs van der Vegt
 *
 * @type {[{password: string, roles: string[], id: string, secret: string, email: string, username: string}, {password: string, roles: string[], id: string, secret: string, email: string, username: string}, {password: string, roles: string[], id: string, secret: string, email: string, username: string}]}
 */

const users = [
    {
        id: "dca4427f-6a14-466e-8a4c-3b2bdefdd492",
        username: "Web-Tech Admin",
        email: "WebTech@Admin.com",
        password: "$2a$12$aB5kTwBCQ/HludvSDdh9f.MhY82T8eV2uwccOM1SttkvzYqqonrYq", //"GoodPassword123"
        secret: "f03771dc-1aa5-4bc7-8a58-38f375aa24a6",
        roles: ["admin", "user"]
    },
    {
        id: "aafa0531-b864-4066-9d0f-2639a1cd21c3",
        username: "Web-Tech User",
        email: "WebTech@User.com",
        password: "$2a$12$5jikto0s0e.HgOtVylwcGeP5Prg9STCZyZcYNPSsdsp6M2pI2thxG", //"VeryStrongPassword123"
        secret: "7de36222-4939-40b8-8183-03043ceed99d",
        roles: ["user"]
    },
]

export default users;