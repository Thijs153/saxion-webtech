import Express from "express";
import cors from "cors";
import movieRouter from "./Routers/movieRouter.js";
import bidRouter from "./Routers/bidRouter.js";
import userRouter from "./Routers/userRouter.js";
import auctionRouter from "./Routers/auctionRouter.js";
import credentialsRouter from "./Routers/credentialRouter.js";
import mediaRouter from "./Routers/mediaRouter.js"
import bodyParser from "body-parser";

const app = Express();
app.use(cors()); // enable pre-flight
const port = 3000;
app.use(Express.json({limit: '20mb'}));
app.use(Express.urlencoded({ extended: true}));
app.use(bodyParser.json())

app.use("/movies", movieRouter);
app.use("/bids", bidRouter);
app.use("/users", userRouter);
app.use("/auctions", auctionRouter);
app.use("/credentials", credentialsRouter);
app.use("/media", mediaRouter);

app.listen(port, () => console.log("listening on port " + port))
