/**
 * Movie Entity
 * Created by Mark Scheperman and Thijs van der Vegt
 *
 * @type {[{lengthInMinutes: number, yearOfRelease: number, rating: number, description: string, id: string, title: string, category: string}, {lengthInMinutes: number, yearOfRelease: number, rating: number, description: string, id: string, title: string, category: string}, {lengthInMinutes: number, yearOfRelease: number, rating: number, description: string, id: string, title: string, category: string}]}
 */

const movies = [
    {
        id: "f1512fe2-2c20-40fc-8de4-4f904ab81cf4",
        title: "The Conjuring: The Devil Made me Do It",
        description: "American horror movie from 2021",
        category: "Horror",
        lengthInMinutes: 112,
        yearOfRelease: 2021,
        rating: 10,
        imageLink: 'https://i.imgur.com/n4zctEa.jpeg',
        videoLink: "https://www.youtube.com/embed/h9Q4zZS2v1k",
        isSold: false
    },

    {
        id: "77f4df24-3ec7-41ef-8d93-451caf7c3655",
        title: "Annabelle Comes Home",
        description: "While babysitting the daughter of Ed and Lorraine Warren, a teenager and her friend unknowingly awaken an evil spirit trapped in a doll.",
        category: "Horror",
        lengthInMinutes: 106,
        yearOfRelease: 2019,
        rating: 8,
        imageLink: "https://i.imgur.com/59PBqc4.jpeg",
        videoLink: "https://www.youtube.com/embed/bCxm7cTpBAs",
        isSold: false
    },
    {
        id: "f2673898-f446-447b-98ae-ee18fe1773cf",
        title: "Central Intelligence",
        description: "Central Intelligence is a 2016 American action comedy film directed by Rawson Marshall Thurber and written by Thurber, Ike Barinholtz and David Stassen. The film stars Kevin Hart and Dwayne Johnson as two old high school classmates who go on the run after one of them joins the CIA to save the world from a terrorist who intends to sell satellite codes.",
        category: "Comedy",
        lengthInMinutes: 116,
        yearOfRelease: 2016,
        rating: 7,
        imageLink: 'https://i.imgur.com/PHWqMdU.jpeg',
        videoLink: "https://www.youtube.com/embed/MxEw3elSJ8M",
        isSold: false
    }
]

export default movies;