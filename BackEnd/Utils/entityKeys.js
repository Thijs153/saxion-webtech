/**
 * EntityKeys file
 * Created by Mark Scheperman and Thijs van der Vegt
 *
 * This file provides the keys of all entities.
 * These are used to validate an object.
 * The keys inside the object, are the required keys that should be provided, i.e. when POSTING a user,
 * you should provide the userName, email and a password.
 */


let user = {
    username: "",
    email: "",
    password: "",
} // +id , +secret, +roles

let auction = {
    title: "",
    movie: {},
    creator: "",
    startingPrice: "",
    bids: [],
    endDate: "",
    hasEnded: false
} // +id +hasended +creator

let bid = {
    price: "",
} // +userID

let movie = {
    title: "",
    description: "",
    category: "",
    lengthInMinutes: "",
    yearOfRelease: "",
    rating: "",
    imageLink: "",
    videoLink: "",
}

export const user_keys = Object.keys(user);
export const auction_keys = Object.keys(auction);
export const bid_keys = Object.keys(bid);
export const movie_keys = Object.keys(movie);