import {derived, writable} from "svelte/store";
import api from "../scripts/api.js";
import {paginate} from "../scripts/pagination.js"
import router from "page";
import {notifier} from "@beyonk/svelte-notifications";
import errorStore from "./error.js"

const searchTerm = writable("");
const status = writable("all");
const rawAuctions = writable([]);
const activeCategory = writable({category: "All"});
export const priceRange = writable([]);

const filteredAuctions = derived(
    [searchTerm, status, rawAuctions, activeCategory, priceRange],
    ([$searchTerm, $status, $items, $activeCategory, $priceRange]) => {
        let items = $items;

        if ($priceRange.length > 0) {
            items = items.filter(x => x && withinPriceRange(x))
        }

        if ($activeCategory.category !== "All") {
            items = items.filter(x => x.movie.category === $activeCategory.category)
        }

        if ($status === "active") {
            return items.filter(x => x.title.toLowerCase().includes($searchTerm.toLowerCase()) && !x.hasEnded)
        }

        if ($status === "expired") {
            return items.filter(x => x.title.toLowerCase().includes($searchTerm.toLowerCase()) && x.hasEnded)
        }

        return items.filter(x => x.title.toLowerCase().includes($searchTerm.toLowerCase()))
    }
)

const createAuction = (data) => {
    if (!data.title || !data.startingPrice || !data.endDate) {
        const errorMessage = "All fields are required!";
        errorStore.setError(errorMessage);
        return;
    }

    //Check whether auction with that movie already exists
    let auctions = [];
    rawAuctions.subscribe(value => {
        auctions = value;
    })();

    for (let auction of auctions) {
        if (data.movie.id === auction.movie.id) {
            const errorMessage = "Auction with this movie already exists!"
            errorStore.setError(errorMessage);
            return;
        }
    }

    if (!data.startingPrice < 0) {
        const errorMessage = "Starting Price can not be a negative number!"
        errorStore.setError(errorMessage);
        return;
    }


    api.post("/auctions", data)
        .then(responseData => {
            let tempAuctions = [];

            rawAuctions.subscribe(data => {
                tempAuctions = data;
            })

            tempAuctions.push(responseData);
            setRawAuctions(tempAuctions);

            errorStore.setError(null);

            notifier.success("Auction Created");
            router.redirect("/");

        })
}

const paginatedAuctionList = (page, perPage) => {
    let auctions = [];
    const unsubscribe = filteredAuctions.subscribe(value => {
        auctions = value
    })

    unsubscribe();

    if (auctions.length <= 0) return []
    return paginate(auctions, page, perPage);
}

const fetchData = () => {
    return api.get('/auctions')
        .then(response => {
            setRawAuctions(response);
            setDefaultPriceRange(response);
        })
}

const setSearchTerm = (term) => {
    searchTerm.set("term");
}

const getSearchTerm = () => {
    return searchTerm;
}

const getFilteredAuctions = () => {
    return filteredAuctions
}

const withinPriceRange = (auction) => {
    let range;
    priceRange.subscribe(value => {
        range = value;
    });
    return (auction.startingPrice >= range[0] && auction.startingPrice <= range[1]);
}

const setDefaultPriceRange = (data) => {
    let range = [0, 0];
    if (data.length >= 1) {
        let min = data.reduce(function (prev, curr) {
            return prev.startingPrice < curr.startingPrice ? prev : curr;
        });
        let max = data.reduce(function (prev, curr) {
            return prev.startingPrice > curr.startingPrice ? prev : curr;
        })
        if (min && max) {
            range = [min.startingPrice, max.startingPrice];
        }
    }

    priceRange.set(range);
}

export const setPriceRange = (range) => {
    priceRange.update(oldStore => {
        oldStore = range;
        return oldStore;
    })
}

const setRawAuctions = (data) => {
    rawAuctions.set(data);
}

const getRawAuctions = () => {
    return rawAuctions;
}

const getAuctionByID = (id) => {
    let auction = [];
    rawAuctions.subscribe((value) => {
        for (let rawAuction of value) {
            if (id === rawAuction.id) {
                auction = rawAuction;
                break;
            }
        }
    });
    return auction;
}

const patchAuction = async (patchBody, id) => {
    let tempAuctions = [];

    rawAuctions.subscribe(data => {
        tempAuctions = data;
    })

    let auction = tempAuctions.find(x => x.id === id);

    if (!auction) {
        throw new Error("No auction found with this ID.");
    }
    const fields = ['title', 'endDate', 'hasEnded', 'startingPrice'];
    let body = {};

    fields.forEach((field) => {
        if (patchBody[field]) {
            if (field === 'endDate') {
                if (auction.hasEnded) {
                    throw new Error("Cannot change date of ended auction.")
                }
            }
            body[field] = patchBody[field];
        }
    });

    const response = await api.patch(`/auctions/${auction.id}`, body);
    if(response.id) {
        fields.forEach((field) => {
            if (body[field]) {
                auction[field] = body[field];
            }
        });

        setRawAuctions(tempAuctions);
        notifier.success('Success! This auction has been edited.');
    } else {
        notifier.danger('Error. Failed to edit this auction. Try again later.')
    }
}

const deleteAuction = async (auctionID) => {
    const response = await api.del(`/auctions/${auctionID}`)
    if (response.ok) {
        let tempAuctions = [];

        rawAuctions.subscribe(data => {
            tempAuctions = data.filter(item => item.id !== auctionID);
        })

        setRawAuctions(tempAuctions);
        notifier.success(`Auction with ID ${auctionID} has been deleted.`, {timeout: 5000});
        router.redirect('/');
    } else {
        notifier.danger('Error. This auction could not be deleted. Try again later.');
    }
}

const addBidToAuction = async (id, bid) => {
    let auction = getAuctionByID(id);

    if(!auction) {
        throw new Error("No auction found with this ID.");
    }

    if (!Number.isInteger(bid.price)) {
        const errorMessage = "Price must be a whole number!";
        errorStore.setError(errorMessage);
        return;
    }

    errorStore.setError(null);

    const response = await api.post(`/auctions/${id}/bids`, bid);

    if (response.id) {
        let tempAuctions = [];

        rawAuctions.subscribe(data => {
            tempAuctions = data;
        })

        const foundAuction = tempAuctions.find(x => x.id === auction.id);
        foundAuction.bids.push(response);
        setRawAuctions(tempAuctions);
        notifier.success('Success! Your bid: €' + response.price + ' has been placed with bid ID: ' + response.id)
    } else {
        notifier.danger('Error! Your bid could not be processed. Try again later.')
    }
}

const deleteBidFromAuction = async (bidId, auctionID) => {
    const auction = getAuctionByID(auctionID);
    if(auction.hasEnded) {
        notifier.danger('Cannot delete a bid from an ended auction!');
        return;
    }
    if(!auction.bids) {
        notifier.danger('This auction has no bids!');
        return;
    }
    const response = await api.del(`/auctions/${auctionID}/bids/${bidId}`)
    if (response.ok) {
        let tempAuctions = [];

        rawAuctions.subscribe(data => {
            tempAuctions = data;
        });

        for(let auction of tempAuctions) {
            for(let bid of auction.bids) {
                if(bid.id === bidId) {
                    auction.bids.splice(bid, 1);
                    break;
                }
            }
        }

        setRawAuctions(tempAuctions);
        router.redirect(`/auctions/${auctionID}`);
        notifier.success(`Bid with ID ${bidId} has been deleted.`, {timeout: 5000});
    } else {
        notifier.danger('Error. This bid could not be deleted. Try again later.');
    }
}

const getPriceRange = () => {
    return priceRange;
}

export default {
    activeCategory,
    searchTerm,
    status,
    rawAuctions,
    priceRange,
    getPriceRange,
    paginatedAuctions: paginatedAuctionList,
    filteredAuctions,
    getFilteredAuctions,
    fetchData,
    setRawAuctions,
    getRawAuctions,
    setSearchTerm,
    getSearchTerm,
    setPriceRange,
    createAuction,
    getAuctionByID,
    addBidToAuction,
    patchAuction,
    deleteAuction,
    deleteBidFromAuction
}