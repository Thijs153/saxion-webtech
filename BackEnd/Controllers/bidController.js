/**
 * Controller for the Bid Entity
 * Created by Mark Scheperman and Thijs van der Vegt
 */

import {v4 as uuid} from "uuid";
import auctions from "../Entities/auctions.js";
import {validateObject} from "../Utils/validator.js";
import {bid_keys} from "../Utils/entityKeys.js";

let bidController = {};

/**
 * Function to list the Bids of a requested Auction.
 * When no query params are given, it will return all Bids.
 * When query params are given, it will return all Bids matching the params.
 *
 * @param req
 * @param res
 */
bidController.list = (req, res) => {
    const auction = auctions.find((product) => {
        return req.params.id === product.id;
    });
    if(!auction) {
        return res.status(400).send("No auction found with ID: " + req.params.id)
    }

    let queryParams = req.query;

    // Filtering on the query params, if the query param matches the key of the bid, it'll be added to 'result'
    const result = auction.bids.filter(bid => {
        let isValid = true;
        for(const key in queryParams) {
            isValid = isValid && bid[key] == queryParams[key]
        }
        return isValid;
    });

    return res.json(result);
}

/**
 * Function to get a specific Bid from an Auction.
 * It will search for a Bid matching the url parameter - ID.
 * When the Bid with the given ID does not exist, it'll return an error.
 *
 * @param req
 * @param res
 */
bidController.get = (req, res) => {
    const auction = auctions.find((product) => {
        return req.params.id === product.id;
    });

    const bid = auction.bids.find((product) => {
        return req.params.bidId === product.id
    });

    if(bid) {
        res.status(200).json(bid);
    } else {
        res.status(404).send("Bid not found!");
    }
}

/**
 * Function to post a new Bid on the requested Auction.
 * You can not provide a new ID, as it will be automatically generated.
 *
 * @param req
 * @param res
 * @returns {*}
 */
bidController.post = (req, res) => {
    let bid = req.body;

    if(bid.id) {
        return res.status(400).send("Do not provide ID");
    }
    if(!req.params.id) {
        return res.status(400).send("No auction specified.")
    }
    if(!validateObject(bid, bid_keys)) {
        return res.status(400).send("Wrong data provided");
    }

    // Validate that the price is higher than the current highest bid
    const auction = auctions.find((product) => {
        return req.params.id === product.id;
    });
    if(!auction) {
        return res.status(400).send("No auction found with this ID.")
    }
    if(bid.price <= auction.startingPrice) {
        return res.status(400).send("New bid amount must exceed the starting price of the auction.")
    }
    if(auction.bids.length >= 1) {
        if(bid.price <= auction.bids[auction.bids.length-1].price){
            return res.status(400).send("New bid amount must exceed the current highest bid amount.")
        }
    }

    bid.id = uuid();
    bid.userId = req.user.id;
    bid.date = new Date();
    bid.auctionId = auction.id;
    auction.bids.push(bid);
    res.status(201).json(bid);
}

/**
 * Function to delete the Bid with the given ID of a specific auction.
 * If the Bid does not exist, an error will be send.
 * @param req
 * @param res
 */
bidController.delete = (req, res) => {
    const auction = auctions.find((product) => {
        return req.params.id === product.id;
    });
    if(!auction) {
        return res.status(400).send("No auction found with ID: " + req.params.id)
    }
    let bidIndex = auction.bids.findIndex((product) => {
        return req.params.bidId === product.id
    });

    if(bidIndex === -1) {
        res.status(404).send("Bid not found!");
    } else {
        auction.bids.splice(bidIndex, 1);
        res.sendStatus(200);
    }
}

export default bidController;