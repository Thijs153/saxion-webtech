import router from 'page';
import user from '../store/user'

export default function (ctx, next) {
    let loggedInUser = user.getUser()

    user.user.subscribe(user => {
        loggedInUser = user;
    })

    if(loggedInUser.roles.includes('admin')) {
        next();
    } else {
        router.redirect('/404')
    }
}