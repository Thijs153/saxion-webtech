/**
 * Auction Entity
 * Created by Mark Scheperman and Thijs van der Vegt
 *
 * @type {[{creator: string, hasEnded: boolean, movie: {lengthInMinutes: number, yearOfRelease: number, isSold: boolean, rating: number, description: string, id: string, title: string, category: string}, endDate: Date, bids: [{date: Date, price: number, id: string, userId: string}], id: string, startingPrice: number, title: string}, {creator: string, hasEnded: boolean, movie: {lengthInMinutes: number, yearOfRelease: number, isSold: boolean, rating: number, description: string, id: string, title: string, category: string}, endDate: Date, bids: *[], id: string, startingPrice: number, title: string}, {creator: string, hasEnded: boolean, movie: {lengthInMinutes: number, yearOfRelease: number, isSold: boolean, rating: number, description: string, id: string, title: string, category: string}, endDate: Date, bids: *[], id: string, startingPrice: number, title: string}]}
 */

const auctions = [
    {
        id: "f3c48113-2e98-4ead-b3f7-07e3f19f8c28",
        title: "Selling A Good Movie!",
        movie: {
            id: "f1512fe2-2c20-40fc-8de4-4f904ab81cf4",
            title: "The Conjuring: The Devil Made me Do It",
            description: "American horror movie from 2021",
            category: "Horror",
            lengthInMinutes: 112,
            yearOfRelease: 2021,
            rating: 10,
            imageLink: 'https://i.imgur.com/n4zctEa.jpeg',
            videoLink: "https://www.youtube.com/embed/h9Q4zZS2v1k",
            isSold: false
        },
        creator: "dca4427f-6a14-466e-8a4c-3b2bdefdd492",
        startingPrice: 100,
        bids: [{
            id: "ea2a5e25-29fa-4746-bec4-8b1f9eba4509",
            userId: "aafa0531-b864-4066-9d0f-2639a1cd21c3",
            price: 105,
            auctionId: 'f3c48113-2e98-4ead-b3f7-07e3f19f8c28',
            date: new Date()
        }],
        endDate: new Date("2021-12-18T21:30:00+02:00"),
        hasEnded: false
    },
    {
        id: "e39c4315-45b0-437d-aa07-c770570b0ecb",
        title: "Second movie",
        movie: {
            id: "77f4df24-3ec7-41ef-8d93-451caf7c3655",
            title: "Annabelle Comes Home",
            description: "American horror movie from 2019",
            category: "Horror",
            lengthInMinutes: 106,
            yearOfRelease: 2019,
            rating: 8,
            imageLink: "https://i.imgur.com/59PBqc4.jpeg",
            videoLink: "https://www.youtube.com/embed/bCxm7cTpBAs",
            isSold: false
        },
        creator: "dca4427f-6a14-466e-8a4c-3b2bdefdd492",
        startingPrice: 50,
        bids: [],
        endDate: new Date("2020-09-21T11:30:00+02:00"),
        hasEnded: false
    }
]

export default auctions;