/**
 * Router for the Bid entity
 * Created by Mark Scheperman and Thijs van der Vegt
 */

import Express from "express";
import bidController from "../Controllers/bidController.js";
import isLoggedIn from "../Middleware/is-logged-in.js";

const bidRouter = Express.Router();

/**
 * Where the route = "/bids":
 * You can GET and POST bids
 */
bidRouter
    .route("/")
    .get(isLoggedIn, bidController.list)
    .post(isLoggedIn, bidController.post);

/**
 * Where the route = "/bids/:id
 * You can GET, and DELETE bids
 *
 * We choose not to implement a PUT endpoint as it is not logical to PUT a bid.
 */
bidRouter
    .route("/:id")
    .get(isLoggedIn, bidController.get)
    .delete(isLoggedIn, bidController.delete);

export default bidRouter;