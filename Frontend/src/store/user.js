import { writable } from "svelte/store";
import api from "../scripts/api";
import { validateEmail, validatePassword } from "../scripts/validation";
import router from "page";
import { getItemFromSessionStorage, removeItemFromLocalStorage, setItemInLocalStorage } from "../scripts/storage";
import { decodeToken } from "../scripts/jwt"
import errorStore from "./error.js"

const user = writable(null)
const token = writable(null)
const isAdmin = writable(null)


const login = (data) => {
    if(!data.email || !data.password){
        const errorMessage = "All fields are required."
        errorStore.setError(errorMessage)
        return;
    }

    if(!validateUser(data)) { return }

    api.post("/credentials", data)
        .then(responseData => {
            setToken(responseData.token);

            const payloadUser = decodeToken(responseData.token);
            setUser(payloadUser)

            setIsAdmin(payloadUser.roles.includes('admin'));

            errorStore.setError(null);
            router.redirect("/")
        })
        .catch(err => {
            const errorMessage = "Email and/or Password is incorrect. Try again.";
            errorStore.setError(errorMessage);
        })
}

const register = (data) => {
    if(!data.email || !data.username || !data.password){
        const errorMessage = "All Fields are Required!"
        errorStore.setError(errorMessage)
        return;
    }

    if(!validateUser(data)) { return }

    api.post("/users", data)
        .then(responseData => {
            errorStore.setError(null)
            router.redirect("/login")
        })
}

const logout = () => {
    removeItemFromLocalStorage("jwtToken")
    setUser(null);
    setIsAdmin(false);
}

const validateUser = (data) => {
    if(!validateEmail(data.email)){
        const errorMessage = "Email is not a valid email. Email should look like: 'example@exampleDomain(.com/.nl)'"
        errorStore.setError(errorMessage)
        return false;

    }

    if(!validatePassword(data.password)) {
        const errorMessage = "Password is not a valid password. Passwords must contain at least 1 capital letter and 1 digit, and it must be at least 6 characters."
        errorStore.setError(errorMessage)
        return false;
    }

    return true;
}

const setUser = (data) => {
    user.set(data);
}

const getUser = () => {
    return user
}

const setToken = (data) => {
    setItemInLocalStorage("jwtToken", data)
    token.set(data);
}

const getToken = () => {
    return token;
}

const setIsAdmin = (boolean) => {
    isAdmin.set(boolean);
}

const getIsAdmin = () => {
    return isAdmin;
}

const restoreFromSessionStorage = () => {
    const jwtToken = getItemFromSessionStorage("jwtToken");
    setToken(jwtToken)

    if(jwtToken) {
       const payloadUser = decodeToken(jwtToken);
       setUser(payloadUser);
       setIsAdmin(payloadUser.roles.includes('admin'));
    } else {
        setIsAdmin(false);
    }

}



export default {
    user,
    token,
    isAdmin,
    setIsAdmin,
    getIsAdmin,
    setUser,
    setToken, 
    getUser, 
    getToken,
    login,
    logout,
    register,
    restoreFromSessionStorage
}

