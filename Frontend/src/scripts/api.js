import user from "../store/user";

const get = async (endpoint, params = {}) => {
    try {
        let config = getConfig("GET", {}, params);
        const response = await fetch(getEndpoint(endpoint), config);
        return await response.json();
    } catch (err) {
        return err;
    }
}

const post = async (endpoint, data, params = {},) => {
    try {
        let config = getConfig("POST", data, params)
        const response = await fetch(getEndpoint(endpoint), config)
        return await response.json();
    } catch (err) {
        return err;
    }
}

const put = async (endpoint, data, params = {},) => {
    try {
        let config = getConfig("PUT", data, params)
        const response = await fetch(getEndpoint(endpoint), config);
        return await response.json();
    } catch (err) {
        return err;
    }
}

const patch = async (endpoint, data, params = {},) => {
    try {
        let config = getConfig("PATCH", data, params)
        const response = await fetch(getEndpoint(endpoint), config);
        return await response.json();
    } catch (err) {
        return err;
    }
}


const del = async (endpoint, data, params = {},) => {
    try {
        let config = getConfig("DELETE", {}, params);
        const response = await fetch(getEndpoint(endpoint), config);
        return await response;
    } catch (err) {
        return err;
    }
}

const getEndpoint = (endpoint) => {
    if (endpoint[0] === "/") endpoint = endpoint.substring(1);
    return "http://localhost:3000/" + endpoint
}

const getConfig = (method, data = null, params = null) => {

    method = method.toUpperCase();
    if (method !== "POST" && method !== "GET" && method !== "PUT" && method !== "PATCH" && method !== "DELETE") throw new Error("Specified HTTP method not allowed");
    let config = {
        method,
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
        },
    }

    if (method === "GET" || method === "DELETE") delete config.body;

    let token = null;

    const unsubscribe = user.getToken().subscribe(data => {
        token = data;
    })

    if (token) {
        config.headers["Authorization"] = `Bearer ${token}`
    }

    unsubscribe();
    return config
}

export default {
    get,
    post,
    put,
    del,
    patch
}