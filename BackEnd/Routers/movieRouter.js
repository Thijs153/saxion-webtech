/**
 * Router for the Movie entity
 * Created by Mark Scheperman and Thijs van der Vegt
 */

import Express from "express";
import movieController from "../Controllers/movieController.js";
import isLoggedIn from "../Middleware/is-logged-in.js";

const movieRouter = Express.Router();

/**
 * Where the route = "/movies":
 * You can GET and POST movies
 * Every request requires the user to be logged in.
 */
movieRouter
    .route("/")
    .get(movieController.list)
    .post(isLoggedIn, movieController.post);

/**
 * Where the route = "/movies/:id
 * You can GET, PUT, and DELETE movies
 * Every request requires the user to be logged in.
 */
movieRouter
    .route("/:id")
    .get(movieController.get)
    .put(isLoggedIn, movieController.put)
    .delete(isLoggedIn, movieController.delete)

export default movieRouter;