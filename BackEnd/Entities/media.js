import { v4 as uuid } from "uuid";
import fs from 'fs';

/**
 * Media Entity
 * Created by Mark Scheperman and Thijs van der Vegt
 *
 * @type {[{postDate: Date, type: string, link: string, id: string}]}
 */
export let media = [];

/**
 * Generates a Media object to store in imgurs.json
 *
 * @param type
 * @param link
 * @param albumID
 * @returns {{link, postDate: Date, id: string, type}|null}
 */
export function generateMedia (type, link, albumID) {
    if(link) {
        if(type === "image" || type === "video") {
            let mediaObject = {
                id: uuid(),
                postDate: new Date(),
                type: type,
                link: link
            };
            if(type === "image") {
                mediaObject.album = `https://imgur.com/a/${albumID}`;
            }
            return mediaObject;
        }
    }
    return null;
}

/**
 * Reads data from imgurs.json file and sets the media to the result
 */
async function loadStoredMedia(){
    const storedMedia = await fs.readFileSync('../database/imgurs.json')
    media = (JSON.parse(storedMedia));
}

loadStoredMedia().then(() => console.log("Media database has been loaded."));


