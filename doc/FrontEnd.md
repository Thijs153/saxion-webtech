# Front-End Documentatie

## Omschrijving
<hr/>
Voor deze opdracht wordt een Single Page Application gemaakt voor de gemaakte server. 
Hierbij moet wordt gemaakt van Svelte (https://svelte.dev/). Svelte zelf is alleen bedoeld voor componenten. 
Om routing mogelijk te maken zullen we gebruik maken van Page js (https://visionmedia.github.io/page.js/)

## Installatie handleiding
<hr/>

### PRE
Zorg ervoor dat Node.Js gedownload is. <br/>
-> Download [here](https://nodejs.org/en/download/).


### 1. Downloaden Node Modules (Via IntelliJ)
<u> Front-End: </u> <br/>

1. Open het package.json bestand in het FrontEnd mapje.

IntelliJ zal rechts onderin het scherm een bericht tonen - "Install dependencies".

2. Klik op de optie " run 'npm install' "

-> Alle Node Modules voor de Front-End zullen gedownload worden.

<u> Back-End: </u> <br/>

1. Open het package.json bestand in het BackEnd mapje.

IntelliJ zal rechts onderin het scherm een bericht tonen - "Install dependencies".

2. Klik op de optie " run 'npm install' "

-> Alle Node Modules voor de Back-End zullen gedownload worden.

### 1. Downloaden Node Modules (Via Terminal)
<u> Front-End: </u> <br/>

1. Navigeer naar het FrontEnd mapje. 
2. Voer het command npm i(nstall) uit.

-> Alle Node Modules voor de Front-End zullen gedownload worden.

<u> Back-End: </u> <br/>

1. Navigeer naar het BackEnd mapje. 
2. Voer het command 'npm i(nstall) uit.'

-> Alle Node Modules voor de Back-End zullen gedownload worden.

### 2.  Het opstarten van het Project
Zorg dat de vorige stap is gedaan, en dat dus alle node modules gedownload zijn.

<u> Front-End: </u> <br/>

1. Navigeer naar het FrontEnd mapje.
2. Voer het command 'npm start' uit.

-> De FrontEnd zal opstarten. 

<u> Back-End: </u> <br/>

1. Navigeer naar het BackEnd mapje.
2. Voer het command 'npm start' uit.

-> De BackEnd zal opstarten.


## Samenwerking met de Back-End
<hr/>

Om de front-end te koppelen met de back-end hebben wij gebruik gemaakt van de fetch api. <br/>

Om de code overzichtelijk te houden, en het makkelijker te maken, hebben wij de calls van de fetch api in helper functions gezet in de api.js file.
De calls naar deze api.js file worden gedaan in de verschillende store files. Dus de calls dat betrekking hebben op een auction worden gedaan in auction.js etc.

We hebben er dus voor gekozen om de meeste fetch calls niet in in de componenten zelf te doen.

## Handelingen
<hr/>

Hier worden bepaalde handelingen uitgelegd, zodat het duidelijk waar en wat je precies moet doen.

### Aanmaken Account
Het aanmaken van een Account doe je op de Registratie pagina, hier kom je via de login pagina. <br/>
Voor het aanmaken van een account moet je een E-Mail, Username, en Password invullen. <br/>
Het E-Mail en Password moeten aan bepaalde voorwaarden voldoen. 

### Aanmaken Movie
Voordat je een veiling aan kan maken, moet je als admin eerst een movie 'aanmaken', dit doe je door op de home pagina op de 'movies' knop te drukken. <br/>
Dit zal je redirecten naar de './movies' pagina. Door vervolgens op 'Add Movie' te drukken kom je op de pagina waar je de Movie aanmaakt. 
Na het aanmaken van een Movie wordt je weer geredirect naar de './movies' pagina.

### Aanmaken Auction
Nadat je een Movie hebt aangemaakt kun je deze verkopen. Dit doe je door op de './movies' pagina op de 'sell' knop van de movie klikt die je wil verkopen. <br/>
Deze knop zal je redirecten naar de pagina waar je de auction aan moet maken. 

Let op: Je kan alleen een auction aanmaken van een film waar nog geen auction van is. Je kan een film namelijk niet twee keer verkopen. <br/>
Je krijgt wel de optie op die auction aan te maken, maar wanneer je de form submit, krijg je een 'error' te zien dat het dus niet kan. 

### Aanpassen / Deleten Auction
Het aanpassen / deleten van een auction kun je als admin doen op de Auction Detail pagina. Hier kom je door op de home page op een bepaalde auction te klikken.

## (Component) Structuur per Pagina

### Standaard
<hr/>

Standaard zit er op elke pagina de navbar, en een notificationsdisplay. Alleen op de Login en de Registratie pagina zit geen navbar.
We hebben ervoor gekozen om deze in de App.svelte in te laden, zodat we het niet telkens in elke view hoeven te doen.

De notificationsdisplay wordt gebruikt om notifications te laten zien, bijvoorbeeld na het aanmaken van een movie of een auction.

### Home Pagina
<hr/>

Op de home pagina kun je alle auctions zien / erop filteren - zoeken. 
Je kan filteren op verschillende dingen, zo kan je filteren op de categorie van een film, de price range van de starting price, en of een auction nog actief is of al verlopen is.
Deze pagina zit niet achter een login, omdat het niet gebruikersvriendelijk is, 
dat je perse ingelogd moet zijn om überhaupt iets te kunnen zien op de website.

Wanneer je hier op een auction klikt ga je naar de auction detail pagina. 

In de filterbalk komt er wanneer je een admin bent, de optie om naar de movie pagina te gaan.

Hieronder de component structuur van de home pagina:

![Home Page](./Images/WebTechHome.png) 

### Movies Pagina
<hr/>

Op de movie pagina kun je alle movies zien. Hier kun je alleen komen als je een admin bent. Deze pagina geeft je de mogelijkheid om een nieuwe auction te maken door op 'sell' te klikken bij een movie.
Ook kun je hier als admin een movie toevoegen, die je evt. later kunt verkopen. Het is dus niet perse nodig dat je direct ook een auction aanmaakt wanneer je een movie aanmaakt. 

Hieronder de component structuur van de movies pagina:

![Movies Page](./Images/WebTechMovies.png)

### Login Pagina / Registratie Pagina
<hr/>

Zoals de namen al zeggen kun je op deze pagina's inloggen en registreren. Je komt op de registratie pagina via de login pagina. 
Wanneer je naar een pagina gaat waarvoor je perse ingelogd moet zijn, wordt je automatisch geredirect naar de login pagina. 

De login en registratie pagina maken zelf geen gebruik van andere componenten. Op deze pagina's is alleen maar een form te zien.
Ze maken wel gebruik van dezelfde styling, daarom hebben wij ervoor gekozen om deze styling los te trekken van het component en in een CSS file te zetten.

### CreateAuction / CreateMovie Pagina
<hr/>

Op deze pagina's kun je een auction en een movie aanmaken.

Je komt op de CreateAuction pagina door op de movies pagina op 'sell' bij een movie te klikken. Op de createMovie pagina kom je door op de movies pagina op 'add movie' te klikken.

Voor deze pagina's geldt eigenlijk hetzelfde als voor de login / registratie pagina. Ze maken zelf geen gebruik van aparte components, en ze delen styling.

### Account Pagina
<hr/>

Op de account pagina wordt de informatie van de ingelogde user getoond.
Ook is hier een tabel te zien met de auctions die je gewonnen hebt. Dit wordt vertaald naar 'Bought items'.

### Auction Detail Pagina
<hr/>

Op de auction detail pagina wordt de auction en de data ervan weergegeven. Op deze pagina kun je dus bijv. een bod uitbrengen, de lijst met biedingen op die auction zien, en als admin de auction aanpassen of verwijderen.

Hieronder de component structuur van de Auction Detail Pagina:

![Auction Detail Page](./Images/WebTechAuctionPage2.drawio.png) 

### Page not Found Pagina
<hr/>

Wij hebben ook een fallback pagina gemaakt. Wanneer je een url invoert die niet bestaat kom je op deze pagina terecht, met de optie om terug naar de home pagina te gaan.
Wij hebben dit gemaakt zodat de user weet dat hij iets fout heeft gedaan, en niet op een lege witte pagina komt. 


## Libraries

### @beyonk/svelte-notifications
<hr/>

Het is fijn dat de user weet of hij iets goed of fout doet. Wij gebruiken deze library om te laten weten dat er iets goed is gegaan. Wanneer de user een auction / movie aanmaakt en dit is gelukt, 
wordt er een notification laten zien boven in het scherm. D.m.v. deze library kon dit heel makkelijk.

Link: https://www.npmjs.com/package/@beyonk/svelte-notifications

### @bulatdashiev/svelte-slider
<hr/>

Voor de dubbel slider van het filteren op de prijs van de auctions hebben wij deze library gebruikt. 

Link: https://github.com/BulatDashiev/svelte-slider

### jwt-decode
<hr/>

Voor het decoden van de jwt token hebben wij jwt-decode gebruikt. Zo hoefden wij dit niet handmatig te doen. 
Deze package valideert de token niet!

Link: https://www.npmjs.com/package/jwt-decode

### Page
<hr/>

Voor de routing hebben wij gekozen voor Page JS. Dit komt omdat dit volgens de opdracht moest. Wij hadden in eerste instante svelte-routing gebruikt.

Link: https://www.npmjs.com/package/page

### svelte-confirm
<hr/>

Voor een confirm bericht hebben wij deze package gebruikt. Wij gebruiken dit wanneer wij bijvoorbeeld een auction willen verwijderen. 

Link: https://www.npmjs.com/package/svelte-confirm

### Bootstrap
<hr/>

Ook maken wij gebruik van Bootstrap en Bootstrap icons. Wij gebruiken dit zodat het schrijven van CSS een stukje makkelijker wordt.
Ook hebben wij bootstrap voornamelijk gebruikt van het responsief maken van de website. 



