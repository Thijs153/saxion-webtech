import {writable} from "svelte/store";

const rawCategories = writable([]);

const generateCategories = (auctions) => {
    let categories = [];

    for (let auction of auctions) {
        const foundCategory = auction.movie.category;
        let matching = false;

        for (let cat of categories) {
            if (cat.category === foundCategory) {
                matching = true;
                cat.amount++;
            }
        }

        if (!matching) {
            const newCategory = {
                category: auction.movie.category,
                amount: 1,
            }
            categories.push(newCategory);
        }
    }
    setCategories(categories);
}

const setCategories = (data) => {
    rawCategories.set(data);
}

const getCategories = () => {
    return rawCategories;
}

export default {
    rawCategories,
    generateCategories,
    setCategories,
    getCategories
}