### Get all users
GET http://localhost:3000/users
Authorization: Bearer {{token}}

> {%
client.test("Test if the Response Status equals 200", function() {
    client.assert(response.status === 200, "Response status is not 200!");
});
client.test("Test if the Response is an Array (Getting everything, should return an array)", function() {
   client.assert(Array.isArray(response.body), "Response is not an Array");
});
%}

### Get user with ID "aafa0531-b864-4066-9d0f-2639a1cd21c3"
GET http://localhost:3000/users/aafa0531-b864-4066-9d0f-2639a1cd21c3
Authorization: Bearer {{token}}

> {%
client.test("Test if the Response Status equals 200", function() {
    client.assert(response.status === 200, "Response status is not 200!");
});
client.test("Request executed successfully", function() {
    client.assert(response.body.id === "aafa0531-b864-4066-9d0f-2639a1cd21c3", "Requested ID and Given ID don't match");
 });
 %}

### Get a user that does not exist
GET http://localhost:3000/users/id_that_doesnt_exist
Authorization: Bearer {{token}}

> {%
client.test("Test if the Response Status equals 404", function() {
   client.assert(response.status === 404, "Response status is not 404!");
});
 %}

### Post a new user (Without providing an ID)
POST http://localhost:3000/users
Content-Type: application/json

{
  "username": "test",
  "email": "test@gmail.com",
  "password": "ImpossibleToCrack123"
}

> {%
client.test("Test if the Response Status equals 201", function() {
   client.assert(response.status === 201, "Response status is not 201");
});
 %}

### Post a new user (Providing an ID)
POST http://localhost:3000/users
Content-Type: application/json

{
  "id": "test",
  "username": "test",
  "email": "test@gmail.com",
  "password": "GoodPassword"
}

> {%
client.test("Test if the Response Status equals 400", function() {
    client.assert(response.status === 400, "Response status is not 400");
});
 %}

### Post a new user (Providing an invalid email)
POST http://localhost:3000/users
Content-Type: application/json

{
  "username": "test",
  "email": "invalidEmail",
  "password": "GoodPassword1",
}

> {%
client.test("Test if the Response Status equals 400", function() {
   client.assert(response.status === 400, "Response status is not 400");
});
 %}

### Post a new user (Providing an invalid password)
POST http://localhost:3000/users
Content-Type: application/json

{
  "username": "Laurens",
  "email": "laurens@gmail.com",
  "password": "invalidpassword",
}

> {%
client.test("Test if the Response Status equals 400", function() {
   client.assert(response.status === 400, "Response status is not 400");
});
 %}

### Update user with ID: aafa0531-b864-4066-9d0f-2639a1cd21c3 (Without providing an ID)
PUT http://localhost:3000/users/aafa0531-b864-4066-9d0f-2639a1cd21c3
Content-Type: application/json
Authorization: Bearer {{token}}

{
  "username": "updated",
  "email": "updated@gmail.com",
  "password": "GoodPassword1"
}

> {%
client.test("Test if the Response Status equals 200", function() {
    client.assert(response.status === 200, "Response Status is not 200!");
});
client.test("Test if the title gets updated", function() {
    client.assert(response.body.username === "updated", "Title hasn't been updated (If the title previously was not 'updated'");
});
 %}

### Update user with ID: aafa0531-b864-4066-9d0f-2639a1cd21c3 (Providing an ID)
PUT http://localhost:3000/users/aafa0531-b864-4066-9d0f-2639a1cd21c3
Content-Type: application/json
Authorization: Bearer {{token}}

{
  "id": "test",
  "username": "updated",
  "email": "updated@gmail.com",
  "password": "GoodPassword",
}

> {%
client.test("Test if the Response Status equals 400", function() {
    client.assert(response.status === 400, "Response Status is not 400!");
});
%}

### Delete user with ID "aafa0531-b864-4066-9d0f-2639a1cd21c3"
DELETE http://localhost:3000/users/aafa0531-b864-4066-9d0f-2639a1cd21c3
Authorization: Bearer {{token}}

> {%
client.test("Test if the Response Status equals 200", function() {
    client.assert(response.status === 200, "Response Status is not 200!");
});
 %}

### Delete a user that does not exist
DELETE http://localhost:3000/users/id_that_doesnt_exist
Authorization: Bearer {{token}}

> {%
client.test("Test if the Response Status equals 404", function() {
    client.assert(response.status === 404, "Response Status is not 404!");
})
 %}
