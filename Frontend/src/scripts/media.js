    import user from "../store/user";

const postMedia = async (base64string, type) => {
    let token = null;

    const unsubscribe = user.getToken().subscribe(data => {
        token = data;
    })

    base64string = trim(base64string);
    let body = {
        base64: base64string,
        type: type
    }

    let response;
    response = await fetch('http://localhost:3000/media', {
        method: "POST",
        body: JSON.stringify(body),
        headers: {
            "Accept": "*/*",
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
        }
    });

    unsubscribe();
    return await response.json();
}

function trim(base64) {
    return /,(.+)/.exec(base64)[1];
}

export default {
    postMedia
}

