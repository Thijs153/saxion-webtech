import {writable} from "svelte/store";

const error = writable(null);

const setError = (message) => {
    error.set(message);
}

const getError = () => {
    return error
}

export default {
    error,
    getError,
    setError
}