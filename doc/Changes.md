#Veranderingen t.o.v. eerste inlevering
In deze file worden de veranderingen t.o.v. de eerste inlevering weergegeven. 
Hierdoor wordt het nakijken voor de docent wat makkelijker.

Hiervoor zullen wij ook verwijzen naar de rubrics van de eerste inlevering.

<hr/>

## Back-end

### REST principes
In de backend ging er iets mis met query parameters wanneer de waarde iets anders was dan een String. 
Bijvoorbeeld de startingPrice. Dit is aangepast.

### Login
Max behaalde punten (geen specifieke veranderingen)

### Security
Max behaalde punten (geen specifieke veranderingen)

### Test coverage
Max behaalde punten (geen specifieke veranderingen)

### API functionality
In de backend kon je niet filteren op minimaal 3 verschillende dingen. Dit hebben wij aangepast, je kan nu ook bij het filteren in auctions filteren op attributen van de movie in een auction. 
Dus bijvoorbeeld op de titel, releaseYear etc. 
  
### Code quality
Max behaalde punten (geen specifieke veranderingen)

<hr/>

## Front-end

### Documentation
De enige feedback die we bij dit kopje hebben gekregen was 'Mooi in markdown', maar omdat we niet de volledige punten hebben behaald was het voor ons niet echt duidelijk wat er specifiek toegevoegd / veranderd moest worden. 

Maar, wij hebben een installatie handleiding toegevoegd die ervoor moet zorgen dat mensen die dit project gaan gebruiken makkelijk het project op kunnen zetten, en kunnen beginnen. 

### Error handling
Wij hadden als feedback dat het nadeel van één plek voor je meldingen het onduidelijk maakt bij welk de melding hoort en dat de meldingen niet specifiek genoeg waren. 
Wij hebben er wel voor gekozen om de meldingen op 1 plek te houden (bovenaan de form), maar wij hebben de meldingen specifieker gemaakt.
Zo zegt bijvoorbeeld een melding niet alleen meer 'Password is not valid' maar geeft de melding gelijk aan wat er dan wel verwacht wordt.
  
Omdat de 'label' van een invoer veld in eerste instantie aangeven met placeholders hebben wij er nu voor gekozen om er toch labels bij te zetten. 
Wij hebben hiervoor gekozen omdat deze placeholders niet meer te zien zijn wanneer de invoervelden zijn ingevoerd, en dan is het niet meer duidelijk wat precies wat is.
  
### Communication Back-end
Wij hebben voor calls met de back-end helper functies gemaakt. Deze zijn te vinden in het api.js bestand in het mapje 'scripts'. Dit zijn funcites voor de GET, PUT, DELETE, POST en PATCH calls. 
Wij willen dat het aanroepen van deze functies maar op 1 plek gebeurd, en dat is in de specifieke stores (.js files). Dus een call voor een auction wordt gedaan in de auction file etc.

### Code Quality
Max Behaalde punten (geen specifieke veranderingen).

### Svelte Concepts
Wij kregen als commentaar dat we te veel stores gebruiken.
Dit klopte ook, wij hadden bijvoorbeeld namelijk voor elke specifieke entity een eigen error store, en voor sommige zelfs meerdere soorten error stores.

Hier hebben wij nu één error store voor gemaakt, te vinden in error.js in het mapje stores, die overal wordt gebruikt. Dit scheelt al een heel aantal stores.

<hr/>

## Extra
Wij moesten nog een keer extra vermelden dat wij de bonus van het toevoegen van een image / trailer ook hebben geïmplementeerd, dus bij deze :)

Het valideren van de user wordt nu in een helper functie gedaan, voorheen werd 2x hetzelfde gedaan bij het inloggen en registreren (in de functies zelf). 
Dit is ook het geval voor het valideren van een movie. 

