/**
 * Controller for the Auction Entity
 * Created by Mark Scheperman and Thijs van der Vegt
 */

import {v4 as uuid} from "uuid";
import auctions from "../Entities/auctions.js"
import bidController from "./bidController.js";
import {validateObject} from "../Utils/validator.js";
import {auction_keys} from "../Utils/entityKeys.js";

let auctionController = {};

/**
 * Function to list the auctions.
 * When no query params are given, it will return all auctions.
 * When query params are given, it will return all auctions matching the params.
 *
 * @param req
 * @param res
 */
auctionController.list = (req, res) => {
    let queryParams = req.query;

    // Update hasEnded status
    for (let auction of auctions) {
        if (new Date(auction.endDate).getTime() - new Date().getTime() < 0) {
            auction.hasEnded = true;
        }
    }

    // Filtering on the query params, if the query param matches the key of the auction, it'll be added to 'result'
    const result = auctions.filter(auction => {
        let isValid = true;
        for (const key in queryParams) {
            isValid = isValid && (auction[key] == queryParams[key] || auction.movie[key] == queryParams[key])
        }
        return isValid;
    });

    res.json(result);
}

/**
 * Function to get a specific Auction.
 * It will search for an auction matching the url parameter - ID.
 * When the auction with the given ID does not exist, it'll return an error.
 *
 * @param req
 * @param res
 */
auctionController.get = (req, res) => {
    const auction = auctions.find((product) => {
        return req.params.id === product.id
    });

    if (auction) {
        // Update hasEnded status
        if (auction.endDate.getTime() - new Date().getTime() < 0) {
            auction.hasEnded = true;
        }
        res.status(200).json(auction);
    } else {
        res.status(404).send("Auction not found!");
    }
}

/**
 * Function to get all the bids on the Auction with the given ID.
 * Uses the list function from the BidController.
 *
 * @param req
 * @param res
 * @returns {*} the list with bids
 */
auctionController.getBids = (req, res) => {
    req.query = {
        auctionID: req.params.id
    }
    return bidController.list(req, res);
}

/**
 * Function to post a new Auction
 * You can not provide a new ID, as it will be automatically generated.
 *
 * @param req
 * @param res
 * @returns {*}
 */
auctionController.post = (req, res) => {
    let auction = req.body;

    if (auction.id) {
        return res.status(400).send("Do not provide ID");
    }
    if (!validateObject(auction, auction_keys)) {
        return res.status(400).send("Wrong data provided");
    }

    auction.id = uuid();
    auction.creator = req.user.id;
    auction.hasEnded = false;
    auction.bids = [];
    auctions.push(auction);

    res.status(201).json(auction);
}

/**
 * Function to PUT the Auction with the given ID.
 * The ID should not change, so you can not provide a new one.
 * @param req
 * @param res
 * @returns {*}
 */
auctionController.put = (req, res) => {
    let newAuction = req.body;

    // Find the index of the Auction with the given ID in the complete Auctions list.
    let auctionIndex = auctions.findIndex((product) => {
        return req.params.id === product.id;
    });

    if (newAuction.id) {
        return res.status(400).send("Do not provide ID");
    }
    if (!validateObject(newAuction, auction_keys)) {
        return res.status(400).send("Wrong data provided");
    }

    newAuction.id = auctions[auctionIndex].id;
    auctions[auctionIndex] = newAuction;

    res.status(200).json(newAuction);
}

/**
 * Function to PATCH the Auction with the given ID.
 * The ID should not change, so you can not provide a new one.
 * @param req
 * @param res
 * @returns {*}
 */
auctionController.patch = (req, res) => {
    let {title, endDate, hasEnded, startingPrice} = req.body;


    // Find the index of the Auction with the given ID in the complete Auctions list.
    let auctionIndex = auctions.findIndex((product) => {
        return req.params.id === product.id;
    });

    if (req.body.id) {
        return res.status(400).send("Do not provide ID");
    }
    if (!validateObject(req.body, auction_keys)) {
        return res.status(400).send("Wrong data provided");
    }

    if(title) {
        auctions[auctionIndex].title = title;
    }

    if(endDate) {
        auctions[auctionIndex].endDate = new Date(endDate);
    }

    if(hasEnded) {
        auctions[auctionIndex].hasEnded = hasEnded;
    }

    if(startingPrice) {
        auctions[auctionIndex].startingPrice = startingPrice;
    }

    res.status(200).json(auctions[auctionIndex]);
}

/**
 * Function to delete the Auction with the given ID.
 * If the Auction does not exist, an error will be send.
 * @param req
 * @param res
 */
auctionController.delete = (req, res) => {
    let auctionIndex = auctions.findIndex((product) => {
        return req.params.id === product.id
    });

    if (auctionIndex === -1) {
        res.status(404).send("Auction not found!");
    } else {
        auctions.splice(auctionIndex, 1);
        res.sendStatus(204);
    }
}

export default auctionController;