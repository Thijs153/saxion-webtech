/**
 * Controller for the User Entity
 * Created by Mark Scheperman and Thijs van der Vegt
 */

import {v4 as uuid} from "uuid";
import users from "../Entities/users.js";
import bcrypt from 'bcrypt';
import {validatePassword, validateEmail, validateObject} from "../Utils/validator.js";
import {user_keys} from "../Utils/entityKeys.js";

let userController = {};

/**
 * Function to list the Users.
 * When no query params are given, it will return all Users.
 * When query params are given, it will return all Users matching the params.
 *
 * @param req
 * @param res
 */
userController.list = (req, res) => {
    let queryParams = req.query;

    // Filtering on the query params, if the query param matches the key of the user, it'll be added to 'result'
    const result = users.filter(user => {
        let isValid = true;
        for(const key in queryParams) {
            isValid = isValid && user[key] == queryParams[key]
        }
        return isValid;
    });

    res.json(result);
}

/**
 * Function to get a specific User.
 * It will search for a User matching the url parameter - ID.
 * When the User with the given ID does not exist, it'll return an error.
 *
 * @param req
 * @param res
 */
userController.get = (req, res) => {
    const user = users.find((product) => {
        return req.params.id === product.id
    });

    if(user) {
        res.status(200).json(user);
    } else {
        res.sendStatus(404);
    }
}

/**
 * Function to post a new User
 * You can not provide a new ID, as it will be automatically generated.
 *
 * @param req
 * @param res
 * @returns {*}
 */
userController.post =  async (req, res) => {
    let user = req.body;

    if(user.id) {
        return res.status(400).send("You should not provide an ID");
    }
    if (!validateObject(user, user_keys)) {
        return res.status(400).send("Wrong data provided")
    }
    if(!validateEmail(user.email)) {
        return res.status(400).send("Email is not valid")
    }
    if(!validatePassword(user.password)) {
        return res.status(400).send("Password is not valid")
    }

    user.password = await hashPassword(res, user.password).catch((err) => {
        console.log(err)
    });

    user.id = uuid();
    user.secret = uuid();
    user.roles = ["user"]
    users.push(user);
    res.status(201).json(user);
}

/**
 * Function to PUT the User with the given ID.
 * The ID should not change, so you can not provide a new one.
 *
 * @param req
 * @param res
 * @returns {*}
 */
userController.put = async (req, res) => {
    let newUser = req.body;
    let userIndex = users.findIndex((product) => {
        return req.params.id === product.id
    });

    if(newUser.id) {
        return res.status(400).send("You should not provide an ID");
    }
    if (!validateObject(newUser, user_keys)) {
        return res.status(400).send("Wrong data provided");
    }
    if(!validateEmail(newUser.email)) {
        return res.status(400).send("Email is not valid");
    }
    if(!validatePassword(newUser.password)) {
        return res.status(400).send("Password is not valid");
    }

    newUser.id = users[userIndex].id;
    newUser.secret = users[userIndex].secret;
    newUser.roles = users[userIndex].roles;
    newUser.password = await hashPassword(res, newUser.password).catch((err) => {
        console.log(err);
    });
    users[userIndex] = newUser;
    res.status(200).json(newUser);
}

/**
 * Function to delete the User with the given ID.
 * If the User does not exist, an error will be send.
 * @param req
 * @param res
 */
userController.delete = (req, res) => {
    let userIndex = users.findIndex((product) => {
        return req.params.id === product.id
    });

    if(userIndex === -1) {
        res.status(404).send("User not found!");
    } else {
        users.splice(userIndex, 1);
        res.sendStatus(200);
    }
}

/**
 * Function to hash a password, using Bcrypt
 * @param res
 * @param password The password that will be hashed
 * @returns {Promise<*>}
 */
async function hashPassword(res, password) {
    try {
        return await bcrypt.hash(password, 10);
    } catch {
        res.sendStatus(500);
    }
}

export default userController;