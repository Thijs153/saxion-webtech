/**
 * Controller for handling credentials
 * Created by Mark Scheperman and Thijs van der Vegt
 */

import users from "../Entities/users.js";
import bcrypt from 'bcrypt';
import jwt from "jsonwebtoken";

let credentialController = {}

/**
 * Function to post to "/credentials" -> Logging in
 * @param req
 * @param res
 */
credentialController.post = (req, res) => {
    const { email, password } = req.body;

    if(email && password) {
        const token = login(email, password);
        if(token) {
            res.status(201).send({token: token});
        } else {
            res.status(401).send("Email and/or password incorrect");
        }
    } else {
        res.status(400).send("Required parameters missing");
    }
}

/**
 * Function to Log in and get the token.
 *
 * @param email with which you log in
 * @param password with which you log in
 * @returns {*|boolean} the token or false
 */
const login = (email, password) => {
    const user = users.find((product) => {
        return product.email === email;
    });

    // comparing the given password with the password of the user
    if(user && user.password) {
        const result = bcrypt.compareSync(password, user.password);

        //returning the token
        if(result) {
            return jwt.sign({
                id: user.id,
                username: user.username,
                email: user.email,
                roles: user.roles,
            }, user.secret);
        }
    }

    return false;
};

export default credentialController