// Deze Entity wordt niet gebruikt.

/**
 * Bid Entity
 * Created by Mark Scheperman and Thijs van der Vegt
 *
 * @type {auctionID: string, date: Date, price: number, id: string, userId: string}
 */

const bids = [
    {
        id: "ea2a5e25-29fa-4746-bec4-8b1f9eba4509",
        userId: "aafa0531-b864-4066-9d0f-2639a1cd21c3", // User: Fairy Nuff
        price: 100,
        date: new Date()
    },

    {
        id: "c48fc292-bbf6-4c80-b40c-823c81e3f7d5",
        userId: "29fbe12b-9d62-474e-86b5-764888e84857", // User: Guus van der Vegt
        price: 200,
        date: new Date()
    }
]

export default bids;