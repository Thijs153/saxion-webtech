/**
 * Validator file
 * Created by Mark Scheperman and Thijs van der Vegt
 *
 * Used to validate certain things
 */

/**
 * Function to validate an object, based on given keys
 *
 * @param object the object that needs to be validated
 * @param keys the keys used for validating
 * @returns {boolean} valid || invalid
 *
 * The object should contain and only contain the keys from 'keys'. if the keys are not the same -> return false
 * The values of the keys will also be 'escaped', for security reasons.
 */
export function validateObject(object, keys) {
    let isValid = true;

    Object.keys(object).forEach(key => {
        let val = object[key];
        if(typeof val === 'string' || val instanceof String) {
            object[key] = escapeHtml(object[key]);
        }
        if(!keys.includes(key)) {
            isValid = false;
        }
    });

    return isValid;
}

/**
 * Function to validate an Email
 *
 * @param email to be validated
 * @returns {boolean} valid || invalid
 *
 * Checks if the Email contains an '@',
 * Checks if there is text before the '@', and text after the '@'
 * Checks if the Email ends with either '.com' or '.nl'
 *
 * Automatically converts the Email to lowercase
 */
export function validateEmail(email) {
    const re = /(\w+.)+@(\w*.)+\.(com|nl)/g;
    return re.test(String(email).toLowerCase());
}


/**
 * Function to validate a Password
 *
 * @param password to be validated
 * @returns {boolean} valid || invalid
 *
 * Checks if the Password contains at least 1 lowercase letter
 * Checks if the Password contains at least 1 capital letter
 * Checks if the Password contains at least 1 number
 * Checks if the Password is at least 6 characters long
 */
export function validatePassword(password) {
    const re =/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{6,}$/g;
    return re.test(String(password));
}

/**
 * Function to escape the HTML
 *
 * @param unsafe string to be escaped
 * @returns {*} returns the new string
 *
 * Replaces certain characters with its String variant.
 */
function escapeHtml(unsafe) {
    try {
        return unsafe
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;");
    } catch (err) {
        console.log(err)
    }
}
