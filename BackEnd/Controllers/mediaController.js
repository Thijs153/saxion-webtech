/**
 * Controller to handle media
 * Created by Mark Scheperman and Thijs van der Vegt
 */

import {media, generateMedia} from "../Entities/media.js"
import fs from 'fs';
import FormData from "form-data";
import fetch from "node-fetch";

let mediaController = {};

const clientID = "3aee79f8bb5b29e";
const token = "e386020af32bf20b3d944e4a35b4c24bc678471e";
const imageAlbumID = 'Tms6kzU';
const imageAlbumDeletehash = '4h1gYSGwq1k3c9E';
const imgurApi = "https://api.imgur.com/3/image";
const altImgurAPI = "https://api.imgur.com/3/upload"

/**
 * Function to list all media.
 * When no query params are given, it will return all media.
 * When query params are given, it will return all media matching the params.
 *
 * @param req
 * @param res
 */
mediaController.list = (req, res) => {
    let queryParams = req.query;

    // Filtering on the query params, if the query param matches the key of the media, it'll be added to 'result'
    const result = media.filter(media => {
        let isValid = true;
        for (const key in queryParams) {
            isValid = isValid && media[key] == queryParams[key]
        }
        return isValid;
    });

    res.json(result);
}

/**
 * Function to upload media.
 * Uploads a base64 string (img/video) to Imgur, images get added to the defined album.
 * Generates a media entity that includes the received Imgur link.
 * Stores generated media entities in imgurs.json.
 * Uses 'deletehash' instead of 'albumID' due to album anonymity.
 *
 * @param req
 * @param res
 * @returns {*}
 */
mediaController.post = async (req, res) => {
    let item = req.body;
    let base64string = item.base64;
    let deletehash = '';
    let albumID = '';

    const form = new FormData();
    form.append('image', base64string);
    form.append('type', "base64")

    if (item.type === "image") {
        deletehash = imageAlbumDeletehash;
        form.append('album', deletehash);
        albumID = imageAlbumID;
    }

    const options = {
        url: altImgurAPI,
        method: "POST",
        encoding: 'utf8',
        redirect: 'follow',
        headers: {
            Authorization: `Bearer ${token}`
        },
        body: form
    }

    let newMedia;
    await fetch(altImgurAPI, options)
        .then(response => response.json())
        .then(result => {
            let mediaObject = generateMedia(item.type, result.data.link, albumID);
            media.push(mediaObject);
            fs.writeFile('../database/imgurs.json', JSON.stringify(media), (err => {
            }))
            if(mediaObject !== null) {
                newMedia = mediaObject;
            }
        })
        .catch(error => console.log('error', error));

    if (newMedia) {
        return res.status(201).json(newMedia);
    } else {
        return res.status(400).send("Imgur could not handle this request.")
    }


}

export default mediaController;