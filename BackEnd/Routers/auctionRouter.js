/**
 * Router for the Auction entity
 * Created by Mark Scheperman and Thijs van der Vegt
 */

import Express from "express";
import auctionController from "../Controllers/auctionController.js";
import bidController from "../Controllers/bidController.js";
import isLoggedIn from "../Middleware/is-logged-in.js";
import isAdmin from "../Middleware/is-admin.js"

const auctionRouter = Express.Router();

/**
 * Where the route = "/auctions":
 * You can GET and POST auctions
 * Every request requires the user to be logged in.
 * In order to POST a new auction, the user also needs to be an admin.
 */
auctionRouter
    .route("/")
    .get(auctionController.list)
    .post(isLoggedIn, isAdmin, auctionController.post);

/**
 * Where the route = "/auctions/:id
 * You can GET, PUT, and DELETE auctions
 * Every request requires the user to be logged in.
 * In order to PUT and DELETE an auction, the user also needs to be an admin.
 */
auctionRouter
    .route("/:id")
    .get(auctionController.get)
    .put(isLoggedIn, isAdmin, auctionController.put)
    .patch(isLoggedIn, isAdmin, auctionController.patch)
    .delete(isLoggedIn, isAdmin, auctionController.delete)

/**
 * Where the route = "/auctions/:id/bids"
 * You can GET, POST bids of an auction
 * The request requires the user to be logged in.
 */
auctionRouter
    .route("/:id/bids")
    .get(bidController.list)
    .post(isLoggedIn, bidController.post);

/**
 * Where the route = "/auctions/:id/bids/:bidId"
 * You can GET a specific bid from an auction
 * The request requires the user to be logged in.
 */
auctionRouter
    .route("/:id/bids/:bidId")
    .get(bidController.get)
    .delete(isLoggedIn, bidController.delete);

export default auctionRouter;