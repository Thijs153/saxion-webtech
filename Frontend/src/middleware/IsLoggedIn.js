import router from 'page';
import user from '../store/user'

export default function (ctx, next) {
    let token;

    user.token.subscribe(jwtToken => {
        token = jwtToken;
        user.restoreFromSessionStorage();
    })

    if(token) {
        next();
    }else{
        router.redirect('/login');
    }
}