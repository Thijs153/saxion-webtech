export const getItemFromSessionStorage = (key) => {
    const item = sessionStorage.getItem(key);
    
    return item ? item : false;
}

export const setItemInLocalStorage = (key, item) => {
    if(key && item){
        sessionStorage.setItem(key, item)
    }
}

export const removeItemFromLocalStorage = (key) => {
    if(key) {
        sessionStorage.removeItem(key);
    }
}