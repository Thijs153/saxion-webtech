export const validateEmail = (email) => {
    const regex = /(\w+.)+@(\w*.)+\.(com|nl)/g;
    return regex.test(String(email).toLowerCase())
}

export const validatePassword = (password) => {
    const re =/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{6,}$/g;
    return re.test(String(password));
}