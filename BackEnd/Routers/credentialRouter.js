/**
 * Router for handling credentials -> Logging in
 * Created by Mark Scheperman and Thijs van der Vegt
 */

import Express from "express";
import credentialController from "../Controllers/credentialController.js";

const credentialsRouter = Express.Router();

/**
 * Where the route = "/credentials"
 * You can POST a.k.a log in.
 */
credentialsRouter.post("/", credentialController.post);

export default credentialsRouter