import Express from "express";
import isLoggedIn from "../Middleware/is-logged-in.js";
import mediaController from "../Controllers/mediaController.js"

/**
 * Router to handle media
 * Created by Mark Scheperman and Thijs van der Vegt
 */
const mediaRouter = Express.Router();

/**
 * Where the route = "/media":
 * You can POST media
 * Every request requires the user to be logged in.
 */
mediaRouter
    .route("/")
    .get(isLoggedIn, mediaController.list)
    .post(isLoggedIn, mediaController.post);

export default mediaRouter;
