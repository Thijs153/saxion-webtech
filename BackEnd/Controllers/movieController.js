/**
 * Controller for the Movie Entity
 * Created by Mark Scheperman and Thijs van der Vegt
 */

import { v4 as uuid } from "uuid";
import movies from "../Entities/movies.js";
import {validateObject} from "../Utils/validator.js";
import {movie_keys} from "../Utils/entityKeys.js";

let movieController = {};

/**
 * Function to list the Movies.
 * When no query params are given, it will return all Movies.
 * When query params are given, it will return all Movies matching the params.
 *
 * @param req
 * @param res
 */
movieController.list = (req, res) => {
    let queryParams = req.query;

    // Filtering on the query params, if the query param matches the key of the auction, it'll be added to 'result'
    const result = movies.filter(movie => {
        let isValid = true;
        for(const key in queryParams) {
            isValid = isValid && movie[key] == queryParams[key]
        }
        return isValid;
    });

    res.json(result);
}

/**
 * Function to get a specific Movie.
 * It will search for a Movie matching the url parameter - ID.
 * When the Movie with the given ID does not exist, it'll return an error.
 *
 * @param req
 * @param res
 */
movieController.get = (req, res) => {
    const movie = movies.find((product) => {
        return req.params.id === product.id
    });

    if(movie) {
        res.status(200).json(movie);
    } else {
        res.status(404).send("Movie not found!");
    }
}

/**
 * Function to post a new Movie
 * You can not provide a new ID, as it will be automatically generated.
 *
 * @param req
 * @param res
 * @returns {*}
 */
movieController.post = (req, res) => {
    let movie = req.body;

    if(movie.id) {
        return res.status(400).send("Do not provide ID");
    }
    if(!validateObject(movie, movie_keys)) {
        return res.status(400).send("Wrong data provided");
    }

    movie.id = uuid();
    movie.isSold = false;
    movies.push(movie);
    res.status(201).json(movie);
}

/**
 * Function to PUT the Movie with the given ID.
 * The ID should not change, so you can not provide a new one.
 * @param req
 * @param res
 * @returns {*}
 */
movieController.put = (req, res) => {
    let newMovie = req.body;
    let movieIndex = movies.findIndex((product) => {
        return req.params.id === product.id
    });

    if(newMovie.id) {
        return res.status(400).send("Do not provide ID");
    }
    if(!validateObject(newMovie, movie_keys)) {
        return res.status(400).send("Wrong data provided");
    }
    newMovie.id = movies[movieIndex].id;
    movies[movieIndex] = newMovie;

    res.status(200).json(newMovie);
}

/**
 * Function to delete the Movie with the given ID.
 * If the Movie does not exist, an error will be send.
 * @param req
 * @param res
 */
movieController.delete = (req, res) => {
    let movieIndex = movies.findIndex((product) => {
        return req.params.id === product.id
    });

    if(movieIndex === -1) {
        res.status(404).send("Movie not found!");
    } else {
        movies.splice(movieIndex, 1);
        res.sendStatus(200);
    }
}

export default movieController;