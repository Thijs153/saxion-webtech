# Back End Documentatie

## User Data
<hr/>

Admin Account:
 - Email: WebTech@Admin.com
 - Wachtwoord: GoodPassword123

User Account:
 - Email: WebTech@User.com
 - Wachtwoord: VeryStrongPassword123

Dit is ook te zien in de 'users.js' file onder 'Entities'

## Short API Description
<hr/>
Wij hebben ervoor gekozen om Movies te gaan verkopen. 

De API bestaat uit de volgende Entiteiten:
- Auctions
- Movies
- Bids
- Users

We hebben voor elke entiteit eigen routers en controllers gemaakt, zodat we de code gescheiden houden, en de code voor een specifieke entitiet gezamenlijk.

Verder gebruiken we voor de API Node.Js in combinatie met Express.

De API voldoet aan de REST-principes, wachtwoorden worden gehashed opgeslagen,
voor bepaalde stukken wordt er gebruik gemaakt van Middleware en er is ook commentaar toegevoegd bij bepaalde stukken code, om het beter leesbaar te maken.

## Entity Class Diagram
<hr/>

![Class Diagram](./Images/WebTechClassDiagram.png) <br/>

## Requests / REST Files
<hr/>

Voor elke entity wordt er een eigen router gebruikt, deze router zal het request doorsturen naar de specifieke Controller van de entity.
Voor bepaalde requests wordt er d.m.v. Middleware gecheckt of de gebruiker is ingelogd en / of de user een admin is.

Voor de Requests / Het testen ervan hebben wij de REST files gemaakt. In deze files kun je alle requests zien en wat deze terug zouden moeten geven.
Er zijn tests geschreven voor zowel good weather cases als bad weather cases.

### IMPORTANT! :
Sommige test zijn afhankelijk van elkaar, daarom is het belangrijk dat je de REST files in een bepaalde volgorde uitvoert. 
1. Credentials.rest
2. Bids.rest
3. Users.rest
4. Movies.rest
5. Auctions.rest

In deze volgorde werken de tests allemaal.

## Sequence Diagram
<hr/>
Om een duidelijker te maken hoe het nou precies werkt hebben wij een Sequence Diagram gemaakt voor het maken van een Movie. 
Er zijn natuurlijk veel meer requests, maar ze lijken qua principe redelijk op elkaar. 

### Diagram:

![Sequence Diagram](./Images/WebTechSeqCreateMovie.png) <br/>

Wanneer de User de form heeft gesubmit met Data van de movie, wordt deze data in de front-end eerst gevalideerd.
Hier wordt bijvoorbeeld gekeken of alle velden wel zijn ingevuld, dat nummers wel echt nummers zijn, etc. 

Dan wordt er een POST Request gestuurd naar de API. Hier wordt nogmaals de data gevalideerd.
Vervolgens wordt de nieuwe Movie toegevoegd aan de Array met Movies (onze database), en wordt er een status 201 + de nieuwe Movie in JSON teruggestuurd.

Tot slot wordt op de front-end een notificatie, dat alles gelukt is, laten zien, en wordt de user geredirect naar een andere pagina.

We hebben ervoor gekozen om de data zowel in de front-end als in de back-end te valideren. Twee keer is namelijk veiliger dan 1 keer.
Het handige aan het valideren op de front-end is, dat wanneer de data fout is, de hele request naar de back-end niet eens gestuurd wordt. Dit scheelt tijd en resources.

## Libraries
<hr/>

### Bcrypt
Voor het hashen van wachtwoorden hebben wij Bcrypt gebruikt. Deze library hashed wachtwoorden automatisch, en dit zorgt ervoor dat we dit niet handmatig hoeven te doen.

Link: https://www.npmjs.com/package/bcrypt

### jsonwebtoken
Voor Authenticatie / Autorisatie hebben wij gebruik gemaakt van jsonwebtoken. 
Na authenticatie geeft de server een token, deze token wordt gebruikt om de server te laten zien dat jij al geauthenticeerd bent (dat jij bent wie jij zegt dat je bent)

Link: https://www.npmjs.com/package/jsonwebtoken

### uuidv4
Elke entiteit heeft een uniek id. Hiervoor hebben wij uuidv4 gebruikt, deze zal automatisch een uuid maken, die uniek is voor elke entiteit. 

Link: https://www.npmjs.com/package/uuidv4 

### Overig
De overige libraries zijn voornamelijk gebruikt voor het communiceren met de Imgur API
