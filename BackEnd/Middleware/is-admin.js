import users from "../Entities/users.js";

/**
 * Middleware to check if the user is an Admin or not.
 * Created by Mark Scheperman and Thijs van der Vegt
 */

const isAdmin = (req, res, next) => {
    const userName = req.user.username;

    // Finding the User that matches the username of the user that is logged in (Based on the token)
    const user = users.find((user) => {
        return userName === user.username
    });

    // If the user is an admin, return next, and if the user is not an admin, send an error.
    if(user.roles.includes('admin')) {
        return next();
    }

    res.status(403).send("this action needs admin privileges!")
}
export default isAdmin;